#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>

static int g_count = 0;

static void *thread_fun_1(void *data)
{
    sleep(1);
    g_count++;
    printf("%s g_count: %d\n", __func__, g_count);
}

static void *thread_fun_2(void *data)
{
    sleep(1);
    g_count++;
    printf("%s g_count: %d\n", __func__, g_count);
}

static void *thread_fun_3(void *data)
{
    sleep(1);
    g_count++;
    printf("%s g_count: %d\n", __func__, g_count);
}

int main(int argc, char const *argv[])
{
    pthread_t pid[3];
    pthread_create(&pid[0], NULL, thread_fun_1, NULL);
    pthread_create(&pid[1], NULL, thread_fun_2, NULL);
    pthread_create(&pid[2], NULL, thread_fun_3, NULL);

    pthread_join(pid[0], NULL);
    pthread_join(pid[1], NULL);
    pthread_join(pid[2], NULL);

    return 0;
}

